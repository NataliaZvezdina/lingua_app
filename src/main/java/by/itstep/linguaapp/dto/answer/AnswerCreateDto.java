package by.itstep.linguaapp.dto.answer;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AnswerCreateDto {

    @NotBlank
    private String body;

    @NotNull
    private Boolean correct;
}
