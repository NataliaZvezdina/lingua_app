package by.itstep.linguaapp.dto.category;

import lombok.Data;

@Data
public class CategoryFullDto {

    private Integer id;
    private String name;
}
