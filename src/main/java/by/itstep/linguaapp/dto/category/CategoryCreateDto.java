package by.itstep.linguaapp.dto.category;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CategoryCreateDto {

    @NotBlank
    private String name;
}
