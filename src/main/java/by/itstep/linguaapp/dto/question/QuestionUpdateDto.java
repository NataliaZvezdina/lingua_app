package by.itstep.linguaapp.dto.question;

import by.itstep.linguaapp.entity.QuestionLevel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class QuestionUpdateDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String description;

    @NotNull
    private QuestionLevel level;
}
