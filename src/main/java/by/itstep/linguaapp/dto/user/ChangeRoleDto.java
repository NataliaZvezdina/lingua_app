package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ChangeRoleDto {

    @NotNull
    private Integer userId;

    @NotNull
    private UserRole newRole;
}
