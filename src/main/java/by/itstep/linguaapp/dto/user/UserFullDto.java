package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {

    private Integer id;
    private String name;
    private String email;
    private UserRole role;
    private String country;
    private String phone;
}
