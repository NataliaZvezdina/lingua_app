package by.itstep.linguaapp.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserCreateDto {

    @NotBlank(message = "Name cannot be null and blank")
    private String name;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(min = 8)
    private String password;

    @Size(min = 2, max = 2)
    private String country;

    @Size(min = 7, max = 20)
    private String phone;
}
