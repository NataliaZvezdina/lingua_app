package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @GetMapping("/questions/{id}")
    public QuestionFullDto findById(@PathVariable Integer id) {
        return questionService.findById(id);
    }

    @GetMapping("/questions")
    public List<QuestionShortDto> findAll() {
        return questionService.findAll();
    }

    @PostMapping("/questions")
    public QuestionFullDto create(@RequestBody QuestionCreateDto dto) {
        return questionService.create(dto);
    }

    @PutMapping("/questions")
    public QuestionFullDto update(@RequestBody QuestionUpdateDto dto) {
        return questionService.update(dto);
    }

    @DeleteMapping("/questiona/{id}")
    public void deleteById(@PathVariable Integer id) {
        questionService.deleteById(id);
    }

    @PostMapping("/questions/{questionId}/answers/{answerId}/check")
    public boolean checkAnswer(@PathVariable Integer questionId, @PathVariable Integer answerId,
                       @RequestParam Integer userId) {
        return questionService.checkAnswer(questionId, answerId, userId);
    }

    @GetMapping("/categories/{categoryId}/questions/random")
    public QuestionFullDto getRandomQuestion(@PathVariable Integer categoryId, @RequestParam Integer userId) {
        return questionService.getRandomQuestion(categoryId, userId);
    }
}
