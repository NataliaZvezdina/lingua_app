package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users/{id}")
    public UserFullDto findById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public List<UserFullDto> findAll() {
        return userService.findAll();
    }

    @PostMapping("/users")
    public UserFullDto create(@RequestBody UserCreateDto dto) {
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@RequestBody UserUpdateDto dto) {
        return userService.update(dto);
    }

    @DeleteMapping("/users/{id}")
    public void deleteById(@PathVariable Integer id) {
        userService.deleteById(id);
    }

    @PutMapping("/users/password")
    public void changePassword(@RequestBody ChangePasswordDto dto) {
        userService.changePassword(dto);
    }

    @PutMapping("/users/role")
    public UserFullDto changeRole(@RequestBody ChangeRoleDto dto) {
        return userService.changeRole(dto);
    }

    @PutMapping("/users/{id}/block")
    public void block(@PathVariable Integer id, @RequestHeader("Authorization") String authorization) {
        userService.block(id);
    }
}
