package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.answer.AnswerUpdateDto;
import by.itstep.linguaapp.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @PutMapping("/answers")
    public AnswerFullDto update(@RequestBody AnswerUpdateDto dto) {
        return answerService.update(dto);
    }

    @DeleteMapping("/answers/{id}")
    public void deleteById(@PathVariable Integer id) {
        answerService.deleteById(id);
    }
}
