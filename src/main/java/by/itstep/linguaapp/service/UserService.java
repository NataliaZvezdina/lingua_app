package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.user.*;

import java.util.List;

public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById(int id);

    List<UserFullDto> findAll();

    void deleteById(int id);

    void changePassword(ChangePasswordDto dto);

    UserFullDto changeRole(ChangeRoleDto dto);

    void block(Integer userId);
}
