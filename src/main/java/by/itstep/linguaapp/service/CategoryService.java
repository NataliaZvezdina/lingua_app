package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;

import java.util.List;

public interface CategoryService {

    CategoryFullDto create(CategoryCreateDto dto);

    CategoryFullDto update(CategoryUpdateDto dto);

    CategoryFullDto findById(int id);

    List<CategoryFullDto> findAll();

    void deleteById(int id);
}
