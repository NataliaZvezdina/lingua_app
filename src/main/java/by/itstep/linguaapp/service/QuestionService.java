package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;

import java.util.List;

public interface QuestionService {

    QuestionFullDto create(QuestionCreateDto dto);

    QuestionFullDto update(QuestionUpdateDto dto);

    QuestionFullDto findById(int id);

    List<QuestionShortDto> findAll();

    void deleteById(int id);

    boolean checkAnswer(int questionId, int answerId, int userId);

    QuestionFullDto getRandomQuestion(int categoryId, int userId);
}
