package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.answer.AnswerUpdateDto;

public interface AnswerService {

    AnswerFullDto update(AnswerUpdateDto dto);

    void deleteById(Integer id);
}
