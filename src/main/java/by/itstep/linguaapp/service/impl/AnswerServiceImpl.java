package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.answer.AnswerUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.exception.BusinessLogicException;
import by.itstep.linguaapp.exception.LinguaapEntityNotFoundException;
import by.itstep.linguaapp.mapper.AnswerMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private AnswerMapper answerMapper;

    @Override
    @Transactional
    public AnswerFullDto update(AnswerUpdateDto dto) {
        AnswerEntity answerToUpdate = answerRepository.findById(dto.getId())
                .orElseThrow(() -> new LinguaapEntityNotFoundException("AnswerEntity was not found by id: "
                        + dto.getId()));

        if (!dto.getCorrect() && answerToUpdate.getCorrect()) {
            throw new BusinessLogicException("""
                    Current answer is correct.  
                    Set another answer correct before updating""");
        }

        if (dto.getCorrect() && !answerToUpdate.getCorrect()) {
            removeCorrectFlagFromAnswers(answerToUpdate.getQuestion());
        }

        answerToUpdate.setCorrect(dto.getCorrect());
        answerToUpdate.setBody(dto.getBody());
        AnswerEntity updatedAnswer = answerRepository.save(answerToUpdate);

        AnswerFullDto fullDto = answerMapper.map(updatedAnswer);
        System.out.println("AnswerServiceImpl -> answer was successfully updated");
        return fullDto;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        AnswerEntity answerToDelete = answerRepository.findById(id)
                .orElseThrow(() -> new LinguaapEntityNotFoundException("AnswerEntity was not found by id: "
                        + id));

        if (answerToDelete.getCorrect()) {
            throw new BusinessLogicException("""
                    Current answer is correct.  
                    Set another answer correct before deleting""");
        }

        if (answerToDelete.getQuestion().getAnswers().size() < 3) {
            throw new BusinessLogicException("Question must contain at least 2 answers");
        }

        answerToDelete.setDeletedAt(Instant.now());
        answerRepository.save(answerToDelete);
        System.out.println("AnswerServiceImpl -> answer was successfully deleted");
    }

    private void removeCorrectFlagFromAnswers(QuestionEntity question) {
        for (AnswerEntity answer : question.getAnswers()) {
            if (answer.getCorrect()) {
                answer.setCorrect(false);
                answerRepository.save(answer);
                break;
            }
        }
    }
}
