package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.exception.LinguaapEntityNotFoundException;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionMapper questionMapper;

    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto dto) {
        throwIfInvalidNumberOfCorrectAnswers(dto);

        QuestionEntity questionEntityToSave = questionMapper.map(dto);
        for (AnswerEntity answer : questionEntityToSave.getAnswers()) {
            answer.setQuestion(questionEntityToSave);
        }
        List<CategoryEntity> categoriesToAdd = categoryRepository.findAllById(dto.getCategoryIds());
        questionEntityToSave.setCategories(categoriesToAdd);

        QuestionEntity savedQuestionEntity = questionRepository.save(questionEntityToSave);

        QuestionFullDto fullQuestionDto = questionMapper.map(savedQuestionEntity);
        System.out.println("QuestionServiceImpl -> question was successfully created");
        return fullQuestionDto;
    }


    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto dto) {
        Optional<QuestionEntity> optionalQuestion = questionRepository.findById(dto.getId());
        if (optionalQuestion.isEmpty()) {
            throw new LinguaapEntityNotFoundException("QuestionEntity was not found by id: " + dto.getId());
        }

        QuestionEntity entityToUpdate = optionalQuestion.get();
        entityToUpdate.setId(dto.getId());
        entityToUpdate.setDescription(dto.getDescription());
        entityToUpdate.setLevel(dto.getLevel());

        QuestionEntity updatedQuestion = questionRepository.save(entityToUpdate);

        QuestionFullDto fullDto = questionMapper.map(updatedQuestion);
        System.out.println("QuestionServiceImpl -> question was successfully updated");
        return fullDto;
    }

    @Override
    @Transactional
    public QuestionFullDto findById(int id) {
        QuestionEntity foundQuestion = questionRepository.findById(id)
                .orElseThrow(() -> new LinguaapEntityNotFoundException("QuestionEntity was not found by id: " + id));

        QuestionFullDto fullDto = questionMapper.map(foundQuestion);
        System.out.println("QuestionServiceImpl -> question was successfully found");
        return fullDto;
    }

    @Override
    @Transactional
    public List<QuestionShortDto> findAll() {
        List<QuestionEntity> foundQuestions = questionRepository.findAll();
        List<QuestionShortDto> shortDtos = questionMapper.map(foundQuestions);
        System.out.println("QuestionServiceImpl -> " + shortDtos.size() + " questions were found");
        return shortDtos;
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        QuestionEntity entityToDelete = questionRepository.findById(id)
                .orElseThrow(() -> new LinguaapEntityNotFoundException("QuestionEntity was not found by id: " + id));

        entityToDelete.setDeletedAt(Instant.now());
        for (AnswerEntity answer : entityToDelete.getAnswers()) {
            answer.setDeletedAt(Instant.now());
        }
        questionRepository.save(entityToDelete);
        System.out.println("QuestionServiceImpl -> question was successfully deleted");
    }

    @Override
    @Transactional
    public boolean checkAnswer(int questionId, int answerId, int userId) {
        AnswerEntity answer = answerRepository.findByIdAndQuestionId(answerId, questionId);
        if (answer == null) {
            throw new LinguaapEntityNotFoundException("Answer was not found by id: " + answerId +
                    " in quewtion by id: " + questionId);
        }

        if (answer.getCorrect()) {
            QuestionEntity question = answer.getQuestion();
            UserEntity user = userRepository.getById(userId);
            question.getUsersWhoCompleted().add(user);

            questionRepository.save(question);
        }

        return answer.getCorrect();
    }

    @Override
    @Transactional
    public QuestionFullDto getRandomQuestion(int categoryId, int userId) {
        List<QuestionEntity> foundQuestions = questionRepository
                .findNotCompleted(categoryId, userId);

        if (foundQuestions.isEmpty()) {
            throw new LinguaapEntityNotFoundException("Can't find available questions by category id: " +
                    categoryId);
        }

        int randomIndex = (int) (foundQuestions.size() * Math.random());
        QuestionEntity randomQuestion = foundQuestions.get(randomIndex);
        System.out.println("QuestionServiceImpl ->  random question was successfully found");
        return questionMapper.map(randomQuestion);
    }

    private void throwIfInvalidNumberOfCorrectAnswers(QuestionCreateDto dto) {
        int counter = 0;
        for (AnswerCreateDto answer : dto.getAnswers()) {
            if (answer.getCorrect()) {
                counter++;
            }
        }
        if (counter != 1) {
            throw new ValidationException("Question must contain only one correct answer");
        }
    }
}
