package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.exception.LinguaapEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValueIsTakenException;
import by.itstep.linguaapp.exception.WrongUserPasswordException;
import by.itstep.linguaapp.mapper.UserMapper;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {
        UserEntity entityToSave = userMapper.map(createDto);
        entityToSave.setBlocked(false);
        entityToSave.setRole(UserRole.USER);

        UserEntity foundUser = userRepository.findByEmail(entityToSave.getEmail());
        if (foundUser != null) {
            throw new UniqueValueIsTakenException("Email is not unique");
        }

        UserEntity entity = userRepository.save(entityToSave);
        UserFullDto fullDto = userMapper.map(entity);

        System.out.println("UserServiceImpl -> user was successfully created");
        return fullDto;
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {
        Optional<UserEntity> optionalUser = userRepository.findById(dto.getId());
        if (optionalUser.isEmpty()) {
            throw new LinguaapEntityNotFoundException("UserEntity was not found by id: " + dto.getId());
        }

        UserEntity userToUpdate = optionalUser.get();

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setPhone(dto.getPhone());
        userToUpdate.setCountry(dto.getCountry());
        userToUpdate.setName(dto.getName());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto fullDto = userMapper.map(updatedUser);

        System.out.println("UserServiceImpl -> user was successfully updated");
        return fullDto;
    }

    @Override
    @Transactional
    public UserFullDto findById(int id) {
        UserEntity foundUser = userRepository.findById(id)
                .orElseThrow(() -> new LinguaapEntityNotFoundException("UserEntity was not found by id: " + id));
//        if (optionalUser.isEmpty()) {
//            throw new LinguaapEntityNotFoundException("UserEntity was not found by id: " + id);
//        }

        UserFullDto fullDto = userMapper.map(foundUser);

        System.out.println("UserServiceImpl -> user was successfully found");
        return fullDto;
    }

    @Override
    @Transactional
    public List<UserFullDto> findAll() {
        List<UserEntity> foundUsers = userRepository.findAll();
        List<UserFullDto> fullDtos = userMapper.map(foundUsers);

        System.out.println("UserServiceImpl -> " + fullDtos.size() + " users were found");
        return fullDtos;
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        UserEntity foundUser = userRepository.findById(id)
                .orElseThrow(() -> new LinguaapEntityNotFoundException("UserEntity was not found by id: " + id));

        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);
        System.out.println("UserServiceImpl -> user was successfully deleted");
    }

    @Override
    @Transactional
    public void changePassword(ChangePasswordDto dto) {
        UserEntity userToUpdate = userRepository.findById(dto.getUserId())
                .orElseThrow(() -> new LinguaapEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        if (!userToUpdate.getPassword().equals(dto.getOldPassword())) {
            throw new WrongUserPasswordException("Wrong password");
        }

        userToUpdate.setPassword(dto.getNewPassword());
        userRepository.save(userToUpdate);

        System.out.println("UserServiceImpl -> user's password was successfully updated");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(ChangeRoleDto dto) {
        UserEntity userToUpdate = userRepository.findById(dto.getUserId())
                .orElseThrow(() -> new LinguaapEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));

        userToUpdate.setRole(dto.getNewRole());
        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto fullDto = userMapper.map(updatedUser);

        System.out.println("UserServiceImpl -> user successfully changed role");
        return fullDto;
    }

    @Override
    @Transactional
    public void block(Integer userId) {
        Optional<UserEntity> optionalUser = userRepository.findById(userId);
        if (optionalUser.isEmpty()) {
            throw new LinguaapEntityNotFoundException("UserEntity was not found by id: " + userId);
        }

        UserEntity userToUpdate = optionalUser.get();
        userToUpdate.setBlocked(true);

        userRepository.save(userToUpdate);
        System.out.println("UserServiceImpl -> user was successfully updated");
    }

    private boolean currentUserIsAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            if (authority.getAuthority().equals("ROLE_" + UserRole.ADMIN.name())) {
                return true;
            }
        }
        return false;
    }

    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }
}
