package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.exception.LinguaapEntityNotFoundException;
import by.itstep.linguaapp.mapper.CategoryMapper;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public CategoryFullDto create(CategoryCreateDto dto) {
        CategoryEntity categoryToSave = categoryMapper.map(dto);
        CategoryEntity savedEntity = categoryRepository.save(categoryToSave);

        CategoryFullDto fullDto = categoryMapper.map(savedEntity);

        System.out.println("CategoryServiceImpl -> category was successfully created");
        return fullDto;
    }

    @Override
    @Transactional
    public CategoryFullDto update(CategoryUpdateDto dto) {
        CategoryEntity categoryToUpdate = categoryRepository.findById(dto.getId())
                .orElseThrow(() -> new LinguaapEntityNotFoundException("CategoryEntity was not found by id: "
                        + dto.getId()));

        categoryToUpdate.setName(dto.getName());
        CategoryEntity updatedCategory = categoryRepository.save(categoryToUpdate);

        CategoryFullDto fullDto = categoryMapper.map(updatedCategory);

        System.out.println("CategoryServiceImpl -> category was successfully updated");
        return fullDto;
    }

    @Override
    @Transactional
    public CategoryFullDto findById(int id) {
        CategoryEntity foundCategory = categoryRepository.findById(id)
                .orElseThrow(() -> new LinguaapEntityNotFoundException("CategoryEntity was not found by id: " + id));

        CategoryFullDto fullDto = categoryMapper.map(foundCategory);
        System.out.println("CategoryServiceImpl -> category was successfully found");
        return fullDto;
    }

    @Override
    @Transactional
    public List<CategoryFullDto> findAll() {
        List<CategoryEntity> foundCategories = categoryRepository.findAll();
        List<CategoryFullDto> fullDtos = categoryMapper.map(foundCategories);

        System.out.println("CategoryServiceImpl -> " + fullDtos.size() + " categories were found");
        return fullDtos;
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        CategoryEntity entityToDelete = categoryRepository.findById(id)
                .orElseThrow(() -> new LinguaapEntityNotFoundException("CategoryEntity was not found by id: " + id));

        entityToDelete.setDeletedAt(Instant.now());
        categoryRepository.save(entityToDelete);
        System.out.println("CategoryServiceImpl -> category was successfully deleted");
    }
}
