package by.itstep.linguaapp.exception;

public class LinguaapEntityNotFoundException extends RuntimeException {

    public LinguaapEntityNotFoundException() {
    }

    public LinguaapEntityNotFoundException(String message) {
        super(message);
    }
}
