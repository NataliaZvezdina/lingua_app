package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.user.UserCreateDto;
import by.itstep.linguaapp.dto.user.UserFullDto;
import by.itstep.linguaapp.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserFullDto map(UserEntity entity);

    List<UserFullDto> map(List<UserEntity> entities);

    UserEntity map(UserCreateDto dto);

//    default String[] map(String value) {
//        return value.split("-");
//    }
}
