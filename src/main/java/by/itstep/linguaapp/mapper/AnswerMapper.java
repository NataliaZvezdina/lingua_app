package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AnswerMapper {

    AnswerFullDto map(AnswerEntity entity);

    List<AnswerFullDto> map(List<AnswerEntity> entities);

    AnswerEntity map(AnswerCreateDto dto);
}
