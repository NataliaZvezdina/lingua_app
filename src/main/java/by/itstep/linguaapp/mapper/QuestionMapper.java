package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.entity.QuestionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AnswerMapper.class, CategoryMapper.class})
public interface QuestionMapper {

    QuestionFullDto map(QuestionEntity entity);

    List<QuestionShortDto> map(List<QuestionEntity> entities);

    QuestionEntity map(QuestionCreateDto dto);

}
