package by.itstep.linguaapp.security;

import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(
                    "JwtUserDetailsService -> user was not found by email: " + email);
        }
        return new JwtUserDetails(
                user.getEmail(), user.getPassword(), user.getBlocked(), List.of(user.getRole())
        );
    }
}
