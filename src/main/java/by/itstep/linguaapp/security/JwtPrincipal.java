package by.itstep.linguaapp.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.security.auth.Subject;
import java.security.Principal;
import java.util.Collection;
import java.util.stream.Collectors;

public class JwtPrincipal implements Principal {

    private String userName;
    private Collection<? extends GrantedAuthority> authorities;

    public JwtPrincipal(final String userName, final Collection authorities) {
        this.userName = userName;
        this.authorities = toAuthorities(authorities);
    }

    private Collection<? extends GrantedAuthority> toAuthorities(final Collection<Object> authorities) {
        return authorities
                .stream()
                .filter(authority -> authority instanceof String)
                .map(role -> "ROLE_" + role)
                .map(role -> new SimpleGrantedAuthority((String) role))
                .collect(Collectors.toList());
    }

    @Override
    public boolean implies(final Subject subject) {
        return false;
    }

    @Override
    public String getName() {
        return userName;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
