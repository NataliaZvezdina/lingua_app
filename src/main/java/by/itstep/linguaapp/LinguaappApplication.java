package by.itstep.linguaapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinguaappApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinguaappApplication.class, args);
	}
}
