package by.itstep.linguaapp.entity;

public enum UserRole {

    USER,
    ADMIN
}
