package by.itstep.linguaapp.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name = "users")
@Entity
@Where(clause = "deleted_at IS NULL")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserRole role;

    @Column(name = "blocked")
    private Boolean blocked;

    @Column(name = "country")
    private String country;

    @Column(name = "phone")
    private String phone;

    @ManyToMany(mappedBy = "usersWhoCompleted")
    private List<QuestionEntity> completedQuestions = new ArrayList<>();

    @Column(name = "deleted_at")
    private Instant deletedAt;
}
