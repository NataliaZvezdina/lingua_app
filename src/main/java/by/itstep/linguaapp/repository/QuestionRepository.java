package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepository extends JpaRepository<QuestionEntity, Integer> {

//    @Query(nativeQuery = true, value = """
//                    SELECT  * FROM questions WHERE deleted_at IS NULL
//            """)
    List<QuestionEntity> findAll();

    @Query(nativeQuery = true, value = """
            SELECT * FROM questions q 
            JOIN questions_categories qc ON q.id = qc.question_id 
            JOIN categories c ON qc.category_id = c.id 
            WHERE c.id = :categoryId""")
    List<QuestionEntity> findAllByCategoryId(@Param("categoryId") Integer categoryId);

    @Query(nativeQuery = true, value = """
                    SELECT * FROM questions q
                    JOIN questions_categories qc ON q.id = qc.question_id 
                    WHERE q.id NOT IN 
                    (SELECT question_id FROM users_completed_questions
                    WHERE user_id = :userId)
                    AND qc.category_id = :categoryId
            """)
    List<QuestionEntity> findNotCompleted(Integer categoryId, @Param("userId") Integer userId);
}
