package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    @Query(nativeQuery = true, value = """
                    SELECT  * FROM users WHERE deleted_at IS NULL
            """)
    List<UserEntity> findAll();

    UserEntity findByEmail(String email);

    List<UserEntity> findAllByCountry(String country);

    @Query(value = "SELECT * FROM users WHERE email LIKE %:d AND deleted_at IS NULL", nativeQuery = true)
    List<UserEntity> findAllByEmailDomain(@Param("d") String domain);

    @Modifying
    @Query(value = "UPDATE users SET deleted_at = NOW() WHERE role = 'ADMIN'", nativeQuery = true)
    void deleteAllAdmins();
}
