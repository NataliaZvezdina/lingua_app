package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.AnswerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Integer> {

    AnswerEntity findByIdAndQuestionId(int answerId, int questionId);

}
