package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.user.UserFullDto;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void mapToFullDto_happyPath() {
        //given
        UserEntity entity = new UserEntity();
        entity.setId(11);
        entity.setName("Bob");
        entity.setPassword("12345678");
        entity.setEmail("bobson@gmail.com");
        entity.setRole(UserRole.ADMIN);
        entity.setPhone("+375-25-5555555");
        entity.setCountry("BY");

        //when
        UserFullDto dto = userMapper.map(entity);

        //then
        Assertions.assertThat(dto).isNotNull();
        System.out.println(dto);
    }
}