package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.user.UserFullDto;
import by.itstep.linguaapp.entity.*;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.QuestionService;
import com.github.javafaker.Faker;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
class QuestionServiceImplTest {

    private Faker faker = new Faker();

    @Autowired
    private QuestionService questionService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        answerRepository.deleteAllInBatch();
        questionRepository.deleteAllInBatch();
        categoryRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
    }

    @Test
    public void findAll_happyPath() {
        // given
        CategoryEntity existingCategory = addCategoryToDb();

        addQuestionToDb(existingCategory.getId());
        addQuestionToDb(existingCategory.getId());
        addQuestionToDb(existingCategory.getId());
        addQuestionToDb(existingCategory.getId());

        QuestionFullDto fullDto = addQuestionToDb(existingCategory.getId());
        questionService.deleteById(fullDto.getId());

        // when
        List<QuestionShortDto> foundActual = questionService.findAll();

        // then
        Assertions.assertThat(foundActual.size()).isEqualTo(4);
    }

    @Test
    @Transactional
    public void create_happyPath() {
        // given
        CategoryEntity existingCategory = addCategoryToDb();
        QuestionCreateDto questionCreateDto = generateCreateDto(List.of(existingCategory.getId()));

        // when
        QuestionFullDto questionFullDto = questionService.create(questionCreateDto);

        // then
        Assertions.assertThat(questionFullDto).isNotNull();
        Assertions.assertThat(questionFullDto.getId()).isNotNull();

        QuestionEntity createdEntity = questionRepository.getById(questionFullDto.getId());
        Assertions.assertThat(createdEntity.getAnswers().size())
                .isEqualTo(questionCreateDto.getAnswers().size());

        for (AnswerEntity savedAnswer : createdEntity.getAnswers()) {
            Assertions.assertThat(savedAnswer.getId()).isNotNull();
        }

        Assertions.assertThat(createdEntity.getCategories().size())
                .isEqualTo(questionCreateDto.getCategoryIds().size());
    }

    @Test
    @Transactional
    public void getRandomQuestion_happyPath() {
        // given
        CategoryEntity firstCategory = addCategoryToDb();
        CategoryEntity secondCategory = addCategoryToDb();

        QuestionFullDto firstQuestion = addQuestionToDb(firstCategory.getId());

        QuestionFullDto secondQuestion = addQuestionToDb(secondCategory.getId());
        QuestionFullDto thirdQuestion = addQuestionToDb(secondCategory.getId());

        UserEntity user = addUserToDb();

        // when
        QuestionFullDto foundQuestion = questionService.getRandomQuestion(firstCategory.getId(), user.getId());

        // then
        Assertions.assertThat(foundQuestion.getId()).isEqualTo(firstQuestion.getId());
    }

    @Test
    @Transactional
    public void getRandomQuestion_whenOneQuestionCompleted() {
        // given
        CategoryEntity firstCategory = addCategoryToDb();
        CategoryEntity secondCategory = addCategoryToDb();

        QuestionFullDto firstQuestion = addQuestionToDb(firstCategory.getId());
        QuestionFullDto secondQuestion = addQuestionToDb(firstCategory.getId());

        QuestionFullDto thirdQuestion = addQuestionToDb(secondCategory.getId());
        QuestionFullDto fourthQuestion = addQuestionToDb(secondCategory.getId());

        UserEntity user = addUserToDb();

        Integer correctAnswerId = null;
        for (AnswerFullDto answer : secondQuestion.getAnswers()) {
            if (answer.getCorrect()) {
                correctAnswerId = answer.getId();
            }
        }

        questionService.checkAnswer(secondQuestion.getId(), correctAnswerId, user.getId());

        // when
        QuestionFullDto foundQuestion = questionService.getRandomQuestion(firstCategory.getId(), user.getId());

        // then
        Assertions.assertThat(foundQuestion.getId()).isEqualTo(firstQuestion.getId());
    }


    private UserEntity addUserToDb() {
        UserEntity user = new UserEntity();
        user.setName(faker.name().firstName());
        user.setEmail(faker.internet().emailAddress());
        user.setPassword(faker.number().digits(10));
        user.setCountry(faker.country().currencyCode());
        user.setPhone(faker.phoneNumber().cellPhone());
        user.setRole(UserRole.USER);
        user.setBlocked(false);
        System.out.println("Added user to db -> " + user);
        return userRepository.save(user);
    }

    private QuestionFullDto addQuestionToDb(Integer categoryId) {
        QuestionCreateDto createDto = generateCreateDto(List.of(categoryId));
        return questionService.create(createDto);
    }

    private QuestionCreateDto generateCreateDto(List<Integer> categoryIds) {
        QuestionCreateDto questionCreateDto = new QuestionCreateDto();
        questionCreateDto.setDescription(faker.howIMetYourMother().catchPhrase());
        questionCreateDto.setLevel(faker.options().option(QuestionLevel.values()));
        questionCreateDto.setCategoryIds(categoryIds);

        List<AnswerCreateDto> answers = List.of(
                generateAnswerCreateDto(true),
                generateAnswerCreateDto(false),
                generateAnswerCreateDto(false),
                generateAnswerCreateDto(false)
        );
        System.out.println(answers);

        questionCreateDto.setAnswers(answers);
        return questionCreateDto;
    }

    private AnswerCreateDto generateAnswerCreateDto(Boolean correct) {
        AnswerCreateDto answerCreateDto = new AnswerCreateDto();
        answerCreateDto.setBody(faker.howIMetYourMother().character());
        answerCreateDto.setCorrect(correct);
        return answerCreateDto;
    }

    private CategoryEntity addCategoryToDb() {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName(faker.commerce().department());
        return categoryRepository.save(categoryEntity);
    }
}